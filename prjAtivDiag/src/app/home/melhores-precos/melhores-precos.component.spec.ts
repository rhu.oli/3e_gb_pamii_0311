import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MelhoresPrecosComponent } from './melhores-precos.component';

describe('MelhoresPrecosComponent', () => {
  let component: MelhoresPrecosComponent;
  let fixture: ComponentFixture<MelhoresPrecosComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MelhoresPrecosComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MelhoresPrecosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
