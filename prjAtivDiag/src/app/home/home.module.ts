import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { TopoComponent } from './topo/topo.component';
import { SlidesComponent } from './slides/slides.component';
import { MelhoresPrecosComponent } from './melhores-precos/melhores-precos.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
  ],
  declarations: [HomePage, TopoComponent, SlidesComponent, MelhoresPrecosComponent]
})
export class HomePageModule {}
