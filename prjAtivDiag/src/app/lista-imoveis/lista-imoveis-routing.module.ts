import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaImoveisPage } from './lista-imoveis.page';

const routes: Routes = [
  {
    path: '',
    component: ListaImoveisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaImoveisPageRoutingModule {}
